a = 5
b = 10
x = True
y = False
# imprime False
print((x or y) and (a < b))
# imprime True
print((x or y) and not (a < b))
