numero = int(input(
    "Forneça um número inteiro. Verificarei se ele é par (True) ou ímpar (False):\n"))

if numero % 2 == 0:
    print(True)
else:
    print(False)
