from http.server import BaseHTTPRequestHandler, HTTPServer

from usuarios import Usuario

# Criacao de Instancias de Usuarios
annibal = Usuario("annibal", "carrasco_de_roma@gmail.com")
baco = Usuario("baco", "deus_do_vinho@gmail.com")
cesar = Usuario("cesar", "ditador@uol.com.br")

lista_usuarios = [annibal, baco, cesar]

# lista de usuarios em html:
str_users_html = ""
for user in lista_usuarios:
    str_users_html += f"""<tr><th>{user.id}</th><th>{user.nome}</th>
    <th>{user.email}</th><th>{user.senha[0:5]}...</th></tr>"""


class SimpleHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/usuarios":
            # Enviar o status code da resposta:
            self.send_response(200)
            # Enviar cabeçalhos:
            self.send_header("Content-Type", "text/html; charset=utf-8")
            # Finalizar cabeçalhos:
            self.end_headers()
            # Escrever dados para o "socket" (wfile):
            data = """
            <html>
                <head>
                    <style>
                        table {
                            border-collapse: collapse;
                        }

                        td, th {
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }
                    </style>
                </head>
                <h1>Usuários</h1>
                <table>""".encode() + f"""
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Email</th>
                         <th>Senha</th>
                        </tr>
                    </thead>
                    <tbody>{str_users_html}</tbody>
                </table>
            </html>
            """.encode()
            self.wfile.write(data)
            print("Implementar!")


server = HTTPServer(("localhost", 8000), SimpleHandler)
server.serve_forever()
