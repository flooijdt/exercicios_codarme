import hashlib


# funcao que cria senha com um hash do email do usuario
def hash_string(texto):
    return hashlib.sha256(texto.encode()).hexdigest()


class Usuario:
    id = 0

    # construtor de Usuario
    def __init__(self, nome, email) -> None:
        self.nome = nome
        self.email = email
        self.senha = hash_string(email)
        Usuario.id += 1
        self.id = Usuario.id
