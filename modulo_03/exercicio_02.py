valor_compras = float(input("Forneça o valor das compras:"))
desconto = float(input("Forneça a porcentagem do desconto recebido sem acrescentar o símbolo \'%\'."))
quantidade_itens = int(input("Forneça a quantidade de itens comprados:"))

print("O valor final das compras é de R$ {}.".format((1 - desconto/100) * valor_compras))
print("O custo médio de cada item foi de R$ {}.".format(valor_compras / quantidade_itens))