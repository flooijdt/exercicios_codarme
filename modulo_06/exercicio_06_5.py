# Calcula a média de todos os alunos (dicionários)

alunos = [
    {
        "nome": "Alice",
        "nota": 8,
    },
    {
        "nome": "Bob",
        "nota": 7,
    },
    {
        "nome": "Carlos",
        "nota": 9,
    }
]
soma = 0

for n in alunos:
    soma += n["nota"]

media = soma / len(alunos)

print(media)
