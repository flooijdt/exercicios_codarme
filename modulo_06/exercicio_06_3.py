# Calcula a média dos elementos de uma lista de números inteiros

import json

lista_str = input(
    "Forneça uma lista para que seja calculada a média de seus elementos:\n")

lista_list = json.loads(lista_str)

soma = 0

for n in lista_list:
    soma += n

media = soma//len(lista_list)

print(media)
