def reverse_list(lista):
    lista_reversa = []
    for i in range(len(lista)):
        lista_reversa.append(lista[-i-1])
    return lista_reversa

lista = ["a", 5, {1}]

print(reverse_list(lista))