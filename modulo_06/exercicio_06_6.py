import json

lista_str = input(
    "Forneça uma lista para que seja calculado seu maior elemento:\n")

lista_list = json.loads(lista_str)

maior = lista_list[0]

for n in lista_list:
    if n > maior:
        maior = n

print(maior)