# Conta a frequência das letras de uma palavra

palavra = input("Forneça uma palavra para que sejam contabilizadas suas letras:\n")

dict_letras = {}

for char in palavra:
    if char in dict_letras:
        dict_letras[char] += 1
    else:
        dict_letras[char] = 1

print(dict_letras)