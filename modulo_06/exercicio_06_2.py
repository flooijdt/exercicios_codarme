# Calcula a soma dos elementos de uma lista fornecida pelo usuário

import json

lista_str = input(
    "Forneça uma lista para que seja calculada a soma de seus elementos:\n")

soma = 0

lista_list = json.loads(lista_str)

for n in lista_list:
    soma += n

print(soma)
