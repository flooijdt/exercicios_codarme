# Calcula a média das notas dos alunos (tuplas)

alunos = [
    ("Alice", 8),
    ("Bob", 7),
    ("Carlos", 9),
]
soma = 0

for n in alunos:
    soma += n[1]

media = soma / len(alunos)

print(media)
