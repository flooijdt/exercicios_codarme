# Imprime lista de números positivos fornecidos pelo usuário

n = int(input("Insira um número positivo inteiro. Caso deseje terminar o programa, insira um número negativo:\n"))
lista = []
while n >= 0:
    lista.append(n)
    n = int(input("Insira um número positivo inteiro. Caso deseje terminar o programa, insira um número negativo:\n"))
print(lista)
