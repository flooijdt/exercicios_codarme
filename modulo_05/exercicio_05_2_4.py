# Acerte o número

# substitua o número padrão 8 pelo número inteiro desejado:
NUMERO = 8

for num in range(3):
    adv = int(input("Acerte o número oculto x:\n"))

    if adv < NUMERO:
        print("Número fornecido mais baixo que x.")
    elif adv > NUMERO:
        print("Número fornecido mais alto que x.")
    else:
        print("Número correto!")
        break
