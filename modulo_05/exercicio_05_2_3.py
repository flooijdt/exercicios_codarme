# número primo?

n = int(input("Forneça um número inteiro:\n"))
numero = 2
primo = bool

while n > numero > 1:
    if n % numero == 0:
        primo = False
        break
    numero += 1

if n == 0:
        print("0 não é primo!")
elif primo != False:
    print("O número fornecido é primo.")
else:
    print("O número fornecido não é primo.")