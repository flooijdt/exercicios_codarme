# Calculadora

a = float(input("Forneça o primeiro número:\n"))
b = float(input("Forneça o segundo número:\n"))

op = input("Forneça o operador da operação a ser realizada (+, -, /, *, **):\n")

if op is "+":
    print(a + b)
elif op is "-":
    print(a - b)
elif op is "/":
    if b != 0:
        print(a / b)
    else:
        print("Não é possível realizar divisão por zero!")
elif op is "*":
    print(a * b)
elif op is "**":
    print(a ** b)
else:
    print("Houve um erro no cálculo da conta requisitada. Verifique se utilizou dados válidos")
