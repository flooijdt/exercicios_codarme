from ast import Try
from django.db import models
from datetime import date

# Create your models here.


class Categoria(models.Model):
    nome = models.CharField(max_length=256, unique=True)
    descricao = models.CharField(max_length=256, default="descrição não disponível.")

    def __str__(self):
        return f"{self.nome} <{self.id}>"
    
    @classmethod
    def criar_categoria(cls, nome):
        if nome:
            categoria = cls.objects.create(nome=nome)
            categoria.save()
        else:
            raise ValueError("error, string cannot be empty or null")

# Cada Table no Banco de Dados é tratada no Django como uma Classe!
class Evento(models.Model):
    nome = models.CharField(max_length=256)
    categoria = models.ForeignKey(Categoria, on_delete=models.SET_NULL, null=True)
    local = models.CharField(max_length=256, blank=True)
    link = models.CharField(max_length=256, blank=True)
    data = models.DateField(null=True)
    participantes = models.IntegerField(default=0)
    # def __init__(self, nome, categoria, local=None, link=None):
    #     self.nome = nome
    #     self.categoria = categoria
    #     self.local = local
    #     self.link = link
    def __str__(self):
        return f"{self.nome} <{self.id}>"
    
    @classmethod
    def cria_evento(cls, nome, categoria, local, link, data=None, participantes=0):
        if local and link:
            raise ValueError("cannot create event with both local and link")
        else:
            evento = cls.objects.create(nome=nome)
            evento.categoria=Categoria(categoria)
            evento.local=local
            evento.link=link
            evento.data=data
            evento.participantes=participantes
            return evento
    # Desafio do módulo 12, acerca do order_by:
    @classmethod
    def ordem_de_eventos_futuros(cls):
        evento = Evento.objects.filter(data__gte=date.today()).order_by("data")
        return evento

