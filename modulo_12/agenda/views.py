from datetime import date
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse
from agenda.models import Categoria, Evento


# Create your views here.
def listar_eventos(request):
    # buscar os eventos criados no banco de dados
    # exibir um template listando esses eventos
    eventos = Evento.objects.filter(data__gte=date.today())
    return render(
        request=request,
        context={"eventos": eventos},
        template_name="agenda/listar_eventos.html",
    )


def exibir_evento(request, id):
    ##evento = ({"nome": "teste", "categoria": "categoria A", "local": "Rio de Janeiro"},)
    # template = loader.get_template("agenda/exibir_evento.html")
    # rendered_template = template.render(context={"evento": evento}, request=request) #HTML
    # return HttpResponse(rendered_template)
    evento = get_object_or_404(Evento, id=id)
    return render(
        request=request,
        context={"evento": evento},
        template_name="agenda/exibir_evento.html",
    )  # substitúi essas três linhas de cima.

def participar_de_evento(request):
    if request.method == "POST":
        evento_id = request.POST.get("evento_id")
        evento = get_object_or_404(Evento, pk=evento_id)
        evento.participantes += 1
        evento.save()
        # É uma boa prática retornar um HttpResponseRedirect após lidar com POST para
        # evitar operações "duplicadas", por exemplo, se o usuário recarregar a página após
        # o POST, o Chrome vai enviar um novo POST (pois "recarregar" na verdade é realizar
        # a última operação bem sucedida).
        return HttpResponseRedirect(reverse('exibir_evento', args=(evento_id,)))

def listar_categorias(request):
    categorias = Categoria.objects.all()
    return render(
        request=request,
        context={"categorias": categorias},
        template_name="agenda/listar_categorias.html",
    )   

def exibir_categoria(request, id):
    categoria = get_object_or_404(Categoria, id=id)
    contagem = Evento.objects.filter(categoria=id).count()
    return render(
        request=request,
        context={"categoria": categoria, "contagem":contagem}, # Desafio do módulo 12 questão 7.
        template_name="agenda/exibir_categoria.html",)
