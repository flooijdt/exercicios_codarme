# Responde se a pessoa é maior de idade apartit de tuplas ou dicts.

def maior_idade(pessoa):
    maior = False
    if type(pessoa) == tuple:
        if pessoa[1] >= 18:
            maior = True

    if type(pessoa) == dict:
        lista_dict = list(pessoa.values())
        if int(lista_dict[0]) >= 18:
            maior = True

    if maior == True:
        print("A pessoa é maior de idade.")
    if maior == False:
        print("A pessoa é menor de idade.")
    return