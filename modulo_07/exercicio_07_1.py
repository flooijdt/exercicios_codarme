# Número primo?

def e_primo(n):
    numero = 2
    primo = True

    if n == 0:       # 0 não é primo!
        return False

    while n > numero > 1:
        if n % numero == 0:
            primo = False
            break
        numero += 1
    return primo
