# Fatorial

def fatorial(n):
    fact = 1
    if n == 0:
        return 1
    else:
        for i in range(n):
            fact = (i + 1) * fact
        return fact
